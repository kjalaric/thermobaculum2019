'''
Does things with the saved ISFinder searches. Specifically, does this:
> grabs accession number, host and insertion sequence length and outputs this as "insertion_sequence_accessions.csv"

VERY janky. Use at your own peril.

kjalaric@gitlab
'''

import os

IS_SEARCH_DIRECTORY = "insertion_sequence_searches/"
IS_OUTPUT_FILE = "insertion_sequence_accessions.csv"

if __name__ == "__main__":
    with open(IS_OUTPUT_FILE, "w") as fo:
        fo.write("filename,accession,length,host,\n")
        for filename in os.listdir(IS_SEARCH_DIRECTORY):
            with open(IS_SEARCH_DIRECTORY + filename, "r") as fi:
                # this works by finding the table starting with 'Accession number" and going from there.
                # this is the jankiest code I have ever written, but will suffice for now
                accession_starts = list()
                acc_lines = list()
                host_lines = list()
                length_lines = list()

                lines = list(fi.readlines())

                for linenum, line in enumerate(lines):
                    if "Accession number" not in line:
                        pass
                    else:
                        accession_starts.append(linenum)

                for accession_start in accession_starts:
                    acc_lines.append(lines[accession_start + 4])
                    host_lines.append(lines[accession_start + 10])

                # length is a bit easier
                for line in lines:
                    if "IS Length" in line:
                        length_lines.append(line)


                accessions = list()
                hosts = list()
                lengths = list()

                for accession in acc_lines:
                    try:
                        accessions.append(accession.split(">")[1].split("<")[0])
                    except:
                        print("({}) Couldn't parse accession for: {}".format(filename, accession))


                for host in host_lines:
                    try:
                        hosts.append(host.split(">")[2].split("<")[0].strip(" "))
                    except:
                        print("({}) Couldn't parse host for: {}".format(filename, host))

                for length in length_lines:
                    try:  # todo what if host fails to parse?
                        lengths.append(int(length.split(">")[-2].split("<")[0].split(" ")[0]))
                    except:
                        print("({}) Couldn't parse length for: {}".format(filename, length))

                for i in range(len(lengths)):
                    fo.write("{},{},{},{},\n".format(filename, accessions[i], lengths[i], hosts[i]))



