import xml.etree.ElementTree as et

tree = et.parse("insertion_sequences.xml")
root = tree.getroot()

with open("insertion_sequence_ids.txt", "w") as fo:
    for hit in root.iter("Hit"):
        hit_def = hit.find("Hit_def").text
        # print(hit_def)
        fo.write(hit_def + "\n")
