'''
Does things with the saved ISFinder searches. Specifically, does this:
> parses for orf functions
> uses the output of process_insertion_sequence_searches.py to relate filenames to accessions and hosts

kjalaric@gitlab
'''

import os
import csv

IS_SEARCH_DIRECTORY = "insertion_sequence_searches/"
ORF_OUTPUT_FILE = "orfs.txt"
IS_OUTPUT_FILE = "insertion_sequence_accessions.csv"


if __name__ == "__main__":
    # get dictionary relating insertion sequence IDs to accessions and hosts
    accessions = dict()
    hosts = dict()
    
    with open(IS_OUTPUT_FILE) as csvi:
        csv_reader = csv.reader(csvi, delimiter=",")
        next(csv_reader, None)  # skip header
        for row in csv_reader:
            is_id = row[0].split(".")[0]
            accession = row[1]
            host = row[3].split(" ")[0] + row[3].split(" ")[1]
            
            if accession == "":
                accessions[is_id] = "(N/A)"
            else:
                accessions[is_id] = accession
            
            hosts[is_id] = host
    
    # search for orfs
    with open(ORF_OUTPUT_FILE, "w") as fo:
        for filename in os.listdir(IS_SEARCH_DIRECTORY):
            fo.write("==={}===\n".format(filename))
            fo.write("Accession no. {}\n".format(accessions[filename.split(".")[0]]))
            fo.write("{}\n\n".format(accessions[filename.split(".")[0]]))

            
            with open(IS_SEARCH_DIRECTORY + filename, "r") as fi:
                orf_lines = list()
                orfs = list()

                lines = list(fi.readlines())

                for line in lines:
                    if "ORF function" not in line:
                        pass
                    else:
                        orf_lines.append(line)

                for orf_line in orf_lines:
                    guess = orf_line.split(" : ")[-3].split(">")[1].split("<")[0].strip(" ")
                    if guess == "":
                        guess = orf_line.split(" : ")[-4].split(">")[1].split("<")[0].strip(" ")
                    if guess == "":
                        print("WARNING: parse failed for {}".format(filename))
                        guess = "!PARSE_ERROR"
                    orfs.append(guess)

                for i in range(len(orfs)):
                    fo.write("{}: {}\n".format(i, orfs[i]))
                    
            fo.write("\n\n")



