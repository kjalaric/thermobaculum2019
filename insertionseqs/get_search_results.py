'''
Script to automate the searching of insertion sequences found by https://isfinder.biotoul.fr/
Insertion sequences must be in a file called "insertion_sequence_ids.txt", one per line, e.g:
IS692
IS801
ISAra2
etc...

The script searches for each of them in the isfinder database and saves the search as an html file for later analysis.
Files are saved in the directory 'insertion_sequence_searches'.

kjalaric@gitlab
'''

import requests

IDS_FILE = "insertion_sequence_ids.txt"
OUTPUT_DIRECTORY = "insertion_sequence_searches/"
SEARCH_FORMAT = "https://isfinder.biotoul.fr/scripts/ficheIS.php?name={}"

if __name__ == "__main__":
    with open(IDS_FILE, "r") as fi:
        for id in fi.readlines():
            query = id.strip("\n")
            print("Searching {}...".format(query))
            r = requests.get(SEARCH_FORMAT.format(query))
            with open(OUTPUT_DIRECTORY + query + ".html", "w") as fo:
                fo.write(r.text)