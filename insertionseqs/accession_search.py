'''
Script to automatically search NCBI for insertion sequences in the csv produced by process_insertion_sequence_searches.py.
If an entry does not have an insertion sequence, its description is instead added to a file "ISs_with_no_accessions.txt"

Requires biopython.

kjalaric@gitlab
'''

import csv
import requests

OUTPUT_DIRECTORY = "accession_searches/"
NCBI_SEARCH_SITE = "https://www.ncbi.nlm.nih.gov/nuccore/{}"
NO_ACCESSIONS_FILENAME = "ISs_with_no_accessions.txt"

if __name__ == "__main__":
    accessions = list()
    missing_accessions = list()
    with open("insertion_sequence_accessions.csv") as csvi:
        csv_reader = csv.reader(csvi, delimiter=",")
        next(csv_reader, None)  # skip header
        for row in csv_reader:
            if not row[1] == "":
                accessions.append(row[1])
            else:
                missing_accessions.append(row[3])

    missing_accessions = list(dict.fromkeys(missing_accessions))  # remove duplicates to reduce searching

    with open(NO_ACCESSIONS_FILENAME, "w") as fo:
        for x in missing_accessions:
            fo.write(x + "\n")
    
    '''
    for accession in list(dict.fromkeys(accessions)):
        print("Searching for {}...".format(accession))
        r = requests.get(NCBI_SEARCH_SITE.format(accession))
        with open(OUTPUT_DIRECTORY+accession+".html", "w") as fo:
            fo.write(r.text)
    '''