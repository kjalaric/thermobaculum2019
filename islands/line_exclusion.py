'''
Goes through a text file and removes all lines containing words in an exclusion list.

kjalaric@gitlab
'''

exclusion_list = ["thermobaculum", "complete genome", "complete sequence", "genome assembly", "ribosomal", "rRNA",
                  "chromosome", "genome"]

original_lines = 0
reduced_lines = 0
with open("horizontal_gene_transfer.txt", "r") as fi:
    with open("gene_transfer_reduced.txt", "w") as fo:
        for line in fi.readlines():
            original_lines += 1
            line_is_good = True
            for x in exclusion_list:
                if x.upper() in line.upper():
                    line_is_good = False
                    break

            if line_is_good:
                fo.write(line)
                reduced_lines += 1

print("Original: {} lines\nReduced: {} lines".format(original_lines, reduced_lines))