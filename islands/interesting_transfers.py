import os
import xml.etree.ElementTree as et

XML_DIRECTORY = "island_queries/"
OUTPUT_FILE = "interesting_transfers.txt"
OUTPUT_FILE_SPECIES = "interesting_species.txt"

INTERESTING_THRESHOLD = 200

def isSpeciesInteresting(hit_def):
    boring_species = []#["UNCULTURED", "THERMOBACULUM TERRENUM"]
    for x in boring_species:
        if x in hit_def.upper():
            return False
    return True


if __name__ == "__main__":
    interesting_hits = list()
    interesting_species = list()
    interesting_species_with_largest_overlaps = dict()
        
    for filename in os.listdir(XML_DIRECTORY):
        interesting_hits.append(["==={}===\n".format(filename)])
                
        tree = et.parse(XML_DIRECTORY + filename)
        root = tree.getroot()

        for hit in root.iter("Hit"):
            hit_num = hit.find("Hit_num").text
            hit_len = hit.find("Hit_hsps").find("Hsp").find("Hsp_align-len").text
            hit_def = hit.find("Hit_def").text
            hit_id = hit.find("Hit_id").text
            hit_acc = hit.find("Hit_accession").text
            
            if int(hit_len) >= INTERESTING_THRESHOLD:
                interesting_hits[-1].append("{} ({}): {} | {} ({})\n".format(hit_num, hit_len, hit_acc, hit_def, hit_id))
                
                if isSpeciesInteresting(hit_def):
                    species = hit_def.split(" ")[0] + " " + hit_def.split(" ")[1]
                    if species not in interesting_species:
                        interesting_species_with_largest_overlaps[species] = int(hit_len)
                    else:
                        if interesting_species_with_largest_overlaps[species] < int(hit_len):
                            interesting_species_with_largest_overlaps[species] = int(hit_len)
                    
                    
                    interesting_species.append(species)
                    
    
    
    interesting_species = list(dict.fromkeys(interesting_species))  # remove duplicates
                    

        
    # all interesting hits
    with open(OUTPUT_FILE, "w") as fo:
        for x in interesting_hits:
            if len(x) > 1:
                fo.write(x[0])
                for j in x[1:]:
                    fo.write(j)
    
    '''          
    # list of interesting species
    with open(OUTPUT_FILE_SPECIES, "w") as fo:
        for x in interesting_species:
            fo.write("{}\n".format(x))
    '''
                         
    # list of interesting species with size of largest overlaps
    interesting_species_with_largest_overlaps
    
    with open(OUTPUT_FILE_SPECIES, "w") as fo:
        for k, v in sorted(interesting_species_with_largest_overlaps.items(), key = lambda x: x[1], reverse=True):
            fo.write("{}: {}\n".format(k, v))
                    
    