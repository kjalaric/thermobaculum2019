import os
import xml.etree.ElementTree as et

XML_DIRECTORY = "island_queries/"
OUTPUT_FILE = "horizontal_gene_transfer.txt"

if __name__ == "__main__":
    with open(OUTPUT_FILE, "w") as fo:
        for filename in os.listdir(XML_DIRECTORY):
            fo.write("==={}===\n".format(filename))
            
            tree = et.parse(XML_DIRECTORY + filename)
            root = tree.getroot()
    
            for hit in root.iter("Hit"):
                hit_num = hit.find("Hit_num").text
                hit_len = hit.find("Hit_hsps").find("Hsp").find("Hsp_align-len").text
                hit_def = hit.find("Hit_def").text
                hit_id = hit.find("Hit_id").text
                hit_acc = hit.find("Hit_accession").text
                fo.write("{} ({}): {} | {} ({})\n".format(hit_num, hit_len, hit_acc, hit_def, hit_id))
                
            fo.write("\n")