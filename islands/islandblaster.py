'''
Submits islands of interest (separate files as produced by islandhunter) as blast queries and stores the results.
Following this, it attempts to read all blast searches and produce:
> a single file containing all


Requires biopython.

kjalaric@gitlab
'''

import os

from Bio import SeqIO
from Bio.Blast import NCBIWWW

import pickle
import xml.etree.ElementTree as et


ISLAND_DIRECTORY = "islands/"  # directory in which the island fasta files are stored
BLAST_SEARCH_DIRECTORY = "islandblaster_searches/"
SUMMARY_FILE = "islandblast_summary.txt"
INTERESTING_FILE = "interesting_transfers.txt"
SPECIES_FILE = "interesting_species.txt"

INTERESTING_THRESHOLD = 100

def isSpeciesInteresting(hit_def):
    boring_species = []#["UNCULTURED", "THERMOBACULUM TERRENUM"]
    for x in boring_species:
        if x in hit_def.upper():
            return False
    return True

if __name__ == "__main__":
    for filename in os.listdir(ISLAND_DIRECTORY):
        print("Blasting {}...".format(filename))
        num_records = 0
        for record in SeqIO.parse(ISLAND_DIRECTORY+filename, "fasta"):
            num_records += 1
            b_results = NCBIWWW.qblast('blastn', 'nr', record.seq, megablast=True)
            print(b_results)

            """
            with open("response_{}_{}.pickle".format(filename, num_records), "wb") as picklefile:
                pickle.dump(b_results, picklefile)

            # print(b_results.__dir__())
            """

            with open(BLAST_SEARCH_DIRECTORY + "blast_{}_{}.xml".format(filename, num_records), "w") as fo:
                fo.write(b_results.read())

            tree = et.parse("blast_{}_{}.xml".format(filename, num_records))
            root = tree.getroot()

            for hit in root.iter("Hit"):
                score = hit.find("Hit_hsps").find("Hsp").find("Hsp_score").text
                print("Hit {} (length = {}): {}".format(hit.find("Hit_num").text, score, hit.find("Hit_def").text))

    with open(SUMMARY_FILE, "w") as fo:
        for filename in os.listdir(BLAST_SEARCH_DIRECTORY):
            fo.write("==={}===\n".format(filename))

            tree = et.parse(BLAST_SEARCH_DIRECTORY + filename)
            root = tree.getroot()

            for hit in root.iter("Hit"):
                hit_num = hit.find("Hit_num").text
                hit_len = hit.find("Hit_hsps").find("Hsp").find("Hsp_align-len").text
                hit_def = hit.find("Hit_def").text
                hit_id = hit.find("Hit_id").text
                hit_acc = hit.find("Hit_accession").text
                fo.write("{} ({}): {} | {} ({})\n".format(hit_num, hit_len, hit_acc, hit_def, hit_id))

            fo.write("\n")


    interesting_hits = list()
    interesting_species = list()
    interesting_species_with_largest_overlaps = dict()

    for filename in os.listdir(BLAST_SEARCH_DIRECTORY):
        interesting_hits.append(["==={}===\n".format(filename)])

        tree = et.parse(BLAST_SEARCH_DIRECTORY + filename)
        root = tree.getroot()

        for hit in root.iter("Hit"):
            hit_num = hit.find("Hit_num").text
            hit_len = hit.find("Hit_hsps").find("Hsp").find("Hsp_align-len").text
            hit_def = hit.find("Hit_def").text
            hit_id = hit.find("Hit_id").text
            hit_acc = hit.find("Hit_accession").text

            if int(hit_len) >= INTERESTING_THRESHOLD:
                interesting_hits[-1].append(
                    "{} ({}): {} | {} ({})\n".format(hit_num, hit_len, hit_acc, hit_def, hit_id))

                if isSpeciesInteresting(hit_def):
                    species = hit_def.split(" ")[0] + " " + hit_def.split(" ")[1]
                    if species not in interesting_species:
                        interesting_species_with_largest_overlaps[species] = int(hit_len)
                    else:
                        if interesting_species_with_largest_overlaps[species] < int(hit_len):
                            interesting_species_with_largest_overlaps[species] = int(hit_len)

                    interesting_species.append(species)

    interesting_species = list(dict.fromkeys(interesting_species))  # remove duplicates

    # all interesting hits
    with open(INTERESTING_FILE, "w") as fo:
        for x in interesting_hits:
            if len(x) > 1:
                fo.write(x[0])
                for j in x[1:]:
                    fo.write(j)

    '''          
    # list of interesting species
    with open(OUTPUT_FILE_SPECIES, "w") as fo:
        for x in interesting_species:
            fo.write("{}\n".format(x))
    '''

    # list of interesting species with size of largest overlaps
    interesting_species_with_largest_overlaps

    with open(SPECIES_FILE, "w") as fo:
        for k, v in sorted(interesting_species_with_largest_overlaps.items(), key=lambda x: x[1], reverse=True):
            fo.write("{}: {}\n".format(k, v))