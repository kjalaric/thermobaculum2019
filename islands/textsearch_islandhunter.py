import os

IH_SEARCH_DIRECTORY = "islandhunter/"

query = "atgcgtaaacctctagttgcaggtaattggaaaatgcacaccaattcctctcaggctcaggctttagcatcctctgtcgtatcaaaaatttcatccgtgatggatgttgaggttgttctttgtcccccgtttgtttggcttgaaagagtggcaagtttagttaggggaactgggatacttgtcggagctcaagatacattttgggaggattggggtgcgtacacaggagagatatctcctctgcagctcagtgagatgtgcagctacgtcatagttggtcattccgaaaggcgacatattatcggagaaactgatgagatagtagctaggaaagctagagctgtgtccagagcaggaatggttcctatactagcggttggggagctggagcaggagtacaaagaaggaatctccaaggagatcgttagtaaccagctctctgctgtcctttctaatggttgggaccatgaaatcgtaattgcatatgagcctgtttgggccataggaacaggtctggctgcttcagctctgtatgctcaggatatggcggcgtttataagagagcaacttggttcatatgggctcaatcaagagagtgttagaatactctatacggtgggagcgtcaataggggtaatttcaaagaatttattgatcagaaagatattgatggggctctggtgggaggagccagcctcaaggcggaagaatttgccgagctag"

if __name__ == "__main__":
    for filename in os.listdir(IH_SEARCH_DIRECTORY):
        with open(IH_SEARCH_DIRECTORY + filename, "r") as fi:
            if query in fi.read():
                print("FOUND: {}".format(filename))