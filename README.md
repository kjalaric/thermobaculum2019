# Scripts used in "Genomic Sequencing and Analysis of Thermobaculum strains"
The scripts in this repository are mostly the same as what was used in the study. The only difference is additional comments. Updated versions of the script may be made available at https://gitlab.com/kjalaric/genomics

Most of the scripts in this repository are undocumented and exist only to show what was used in the study. Use at your own peril.

## Directory:

### pcomp - Protein sequence composition comparison tool
Used to count up number of different residues in two different files.

### islands
Scripts used in analysing IslandHunter output.

### insertionseqs
Scripts used in analysing ISFinder output.

### general
Scripts that were used for other purposes.