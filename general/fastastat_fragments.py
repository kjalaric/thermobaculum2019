length = 0
GC = 0
num_frags = 0

with open("BIGBOI.fasta", "r") as fi:
    for line in fi.readlines():
        if line[0] == ">":
            num_frags += 1
            continue
        for base in line:
            if base == "\n":
                continue  # this is usually at the end anyway
            length += 1
            if base.upper() == "G" or base.upper() == "C":
                GC += 1

print("Total number of bases: {}".format(length))
print("Total number of fragments: {}".format(num_frags))
print("Average fragment size: {}".format(length/num_frags))
print("GC content across all reads: {}".format(100.0*GC/length))

print("\n{}".format(length/int(input("Input assembly size to estimate coverage: "))))