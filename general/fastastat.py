length = 0
GC = 0

total_length = 0
total_GC = 0

with open("assembly.fasta", "r") as fi:
    for line in fi.readlines():
        if line[0] == ">":
            try:
                print("Length: {}bp ({:.2f}Mbp), GC%: {}".format(length, length / 1000000.0, 100.0 * GC / length))
            except:
                continue  # div/zero on first run
            total_length += length
            total_GC += GC
            length = 0
            GC = 0
        else:
            for base in line:
                if base == "\n":
                    continue
                length += 1
                if base.upper() == "G" or base.upper() == "C":
                    GC += 1

print("Length: {}bp ({:.2f}Mbp), GC%: {}".format(length, length/1000000.0, 100.0*GC/length))

total_length += length
total_GC += GC
print("\nOverall Length: {}bp ({:.2f}Mbp), GC%: {}".format(total_length, total_length/1000000.0, 100.0*total_GC/total_length))
