count = 0
with open("gene_transfer_reduced.txt", "r") as fi:
    for line in fi.readlines():
        try:
            int(line[0])
            count += 1
        except:
            continue

print(count)