# pcomp - Protein Sequence Composition Comparison

This script can tell you the difference in amino acid compositon between proteins as specified in two files.

For this study, the two files were named "ecoli.txt" and "yellowstone.txt". Running the script in the directory with these files produces the output in "amino_comparison.txt". Changing the filenames in the script is trivial and should allow you to use the script for your purposes.

Sample output:
~~~
Full set of differences (ecoli.txt - yellowstone.txt):
A/Ala: 17
C/Cys: 1
E/Glu: -3
D/Asp: -2
G/Gly: 2
F/Phe: 0
I/Ile: 2
H/His: 4
K/Lys: 7
M/Met: -1
L/Leu: -2
N/Asn: 4
Q/Gln: 0
P/Pro: 0
S/Ser: -9
R/Arg: -4
T/Thr: 2
W/Trp: -4
V/Val: -7
Y/Tyr: -2

Differences in hydrophobic amino acids (ecoli.txt - yellowstone.txt):
A/Ala: 17
G/Gly: 2
F/Phe: 0
I/Ile: 2
M/Met: -1
L/Leu: -2
P/Pro: 0
V/Val: -7

Differences in charged amino acids (ecoli.txt - yellowstone.txt):
E/Glu: -3
D/Asp: -2
K/Lys: 7
R/Arg: -4

Differences in polar amino acids (ecoli.txt - yellowstone.txt):
C/Cys: 1
H/His: 4
N/Asn: 4
Q/Gln: 0
S/Ser: -9
T/Thr: 2
Y/Tyr: -2

Differences in amphipathic/polar amino acids (ecoli.txt - yellowstone.txt):
M/Met: -1
W/Trp: -4
Y/Tyr: -2
~~~