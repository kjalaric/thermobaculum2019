'''
pcomp.py
Script to compare the amino acid sequence of two different proteins and output the results.

Use:
> copy the two files (each containing the protein sequence of interest, with no header) to the script directory
> run script
> inspect "amino_comparison.txt"

Developed by B O'Reilly for "Genomic Sequencing and Analysis of Thermobaculum strains", Seeley and O'Reilly, 2019.
'''

FILE1 = "ecoli.txt"
FILE2 = "yellowstone.txt"
OUTPUT = "amino_comparison.txt"

codons = {'A': 'Ala', 'C': 'Cys', 'D': 'Asp', 'E': 'Glu', 'F': 'Phe', 'G': 'Gly', 'H': 'His',
          'I': 'Ile', 'K': 'Lys', 'L': 'Leu', 'M': 'Met', 'N': 'Asn', 'P': 'Pro', 'Q': 'Gln',
          'R': 'Arg', 'S': 'Ser', 'T': 'Thr', 'V': 'Val', 'W': 'Trp', 'Y': 'Tyr', 'B': 'Asx',
          'X': 'Xaa', 'Z': 'Glx', 'J': 'Xle', 'U': 'Sec', 'O': 'Pyl', "-": "Stop"}  # extended IUPAC copied from Bio.Data.IUPACData

hydrophobic_amino_acids = ['A', 'I', 'L', 'M', 'F', 'V', 'P', 'G']
polar_amino_acids = ['Q', 'N', 'H', 'S', 'T', 'Y', 'C']
amphipathic_amino_acids = ['W', 'Y', 'M']
charged_amino_acids = ['R', 'K', 'D', 'E']

if __name__ == "__main__":
    amino_acid_freqs_1 = dict()
    amino_acid_freqs_2 = dict()

    with open(FILE1, "r") as fi:
        for line in fi.readlines():
            for letter in line:
                try:
                    amino_acid_freqs_1[letter] += 1
                except:
                    amino_acid_freqs_1[letter] = 1

    with open(FILE2, "r") as fi:
        for line in fi.readlines():
            for letter in line:
                try:
                    amino_acid_freqs_2[letter] += 1
                except:
                    amino_acid_freqs_2[letter] = 1

    with open(OUTPUT, "w") as fo:
        fo.write("Full set of differences ({} - {}):".format(FILE1, FILE2))
        for k in codons.keys():
            try:
                fo.write("\n{}/{}: {}".format(k, codons[k], amino_acid_freqs_1[k] - amino_acid_freqs_2[k]))
            except:
                continue

        fo.write("\n\n")
        fo.write("Differences in hydrophobic amino acids ({} - {}):".format(FILE1, FILE2))
        for k in codons.keys():
            if k in hydrophobic_amino_acids:
                try:
                    fo.write("\n{}/{}: {}".format(k, codons[k], amino_acid_freqs_1[k] - amino_acid_freqs_2[k]))
                except:
                    continue

        fo.write("\n\n")
        fo.write("Differences in charged amino acids ({} - {}):".format(FILE1, FILE2))
        for k in codons.keys():
            if k in charged_amino_acids:
                try:
                    fo.write("\n{}/{}: {}".format(k, codons[k], amino_acid_freqs_1[k] - amino_acid_freqs_2[k]))
                except:
                    continue

        fo.write("\n\n")
        fo.write("Differences in polar amino acids ({} - {}):".format(FILE1, FILE2))
        for k in codons.keys():
            if k in polar_amino_acids:
                try:
                    fo.write("\n{}/{}: {}".format(k, codons[k], amino_acid_freqs_1[k] - amino_acid_freqs_2[k]))
                except:
                    continue

        fo.write("\n\n")
        fo.write("Differences in amphipathic/polar amino acids ({} - {}):".format(FILE1, FILE2))
        for k in codons.keys():
            if k in amphipathic_amino_acids:
                try:
                    fo.write("\n{}/{}: {}".format(k, codons[k], amino_acid_freqs_1[k] - amino_acid_freqs_2[k]))
                except:
                    continue
